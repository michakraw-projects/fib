**Zadanie**

Dla danych n i m wyznacz fib(n) mod m

**Wejście**

W pierwszej linii wejścia podana jest liczba testów T (1 ≤ T ≤ 100). W następnych T wierszach podane są pary n, m (1 ≤ n,m ≤ 109).

**Przykład**

Dla danych wejściowych

10

1 10

2 10

3 10

4 25

5 25

6 25

7 27

8 29

9 31

10 33

poprawną odpowiedzią jest

1

1

2

3

5

8

13

21

3

22