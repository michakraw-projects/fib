#include <cstdio>

int main()
{
    int p;
    long long int n,m;
    long long int a,b,c,d,w1,w2,w3,w4;
    scanf("%d", &p);
    for (int i=0;i<p;i++)
    {
        scanf("%lld%lld", &n, &m);   
        
        long long int A[2][2]={{1,1},{1,0}};
        long long int W[2][2]={{1,1},{1,0}};
        

        for (int j=1;j<=n;j<<=1)
        {
            a=A[0][0], b=A[0][1],c=A[1][0],d=A[1][1];
            if((j&n) != 0)
            {
                w1 = W[0][0],w2 = W[0][1],w3 = W[1][0],w4 = W[1][1];
     
                W[0][0] = (w1*a + w2 * c)%m;
                W[1][0] = (w3*a + w4 * c)%m;
                W[0][1] = (w1*b + w2 * d)%m;
                W[1][1] = (w3*b + w4 * d)%m;
            }

            A[0][0] = (a*a + b*c)%m;
            A[1][0] = (c*a + d*c)%m;
            A[0][1] = (a*b + b*d)%m;
            A[1][1] = (c*b + d*d)%m;
        }
        printf("%lld\n", W[1][1]%m);
    } 
    return 0;
}
